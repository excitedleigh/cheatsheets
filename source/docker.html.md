---
title: Docker
category: Tools
---

## Docker CLI

Create a data container

```shell
docker run --name mycontainer-data --entrypoint /bin/true myimage
```

Run a container interactively, discarding it at exit

```shell
docker run -it --rm myimage
```

Run a container as a daemon

```shell
docker run -d --name mycontainer --volumes-from mycontainer-data \
    -e MYENVVAR=myvalue \
    -p 8000:8000 \
    myimage
```

Reclaim unused disk

```shell
docker system prune
```
