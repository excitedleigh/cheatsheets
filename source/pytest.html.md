---
title: pytest
category: Testing Tools
---

# unittest / pytest equivalences

<table>
  <tr><th>unittest</th><th>pytest</th></tr>
  <tr>
    <td>
      <pre lang='python'>self.assertAlmostEqual(a, b)</pre>
    </td>
    <td>
      <pre lang='python'>assert a == pytest.approx(b)</pre>
      <a href="https://docs.pytest.org/en/latest/builtin.html#pytest.approx">docs</a>
    </td>
  </tr>
</table>
