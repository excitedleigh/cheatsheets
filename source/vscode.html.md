---
title: Visual Studio Code
category: Editors
---

## Panel Shortcuts

| Prefix | Keybinding | Panel |
|--------|------------|-------|
| none | Ctrl-P | Files in project |
| `@` | Ctrl-Shift-O | Symbols in file |
| `#` | Ctrl-T | Symbols in project |
| `>` | Ctrl-Shift-P | Command |
| `:` | Ctrl-G | Go to line |
