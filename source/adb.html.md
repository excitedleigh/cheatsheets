---
title: Android Debug Bridge
category: Tools
---

## Connecting ADB wirelessly to a device

- One-time:
    1. Enable developer mode using a cable.
    2. Run `adb tcpip 5555`.
- Per session:
    1. Go to Settings &rarr; Wi-Fi &rarr; top-right menu &rarr; Advanced, and get the IP address from the bottom of the screen.
    1. Run `adb connect <ipaddr>`.
- For React Native apps
    1. Run `adb reverse tcp:8081 tcp:8081` to forward the packager through
- To disable
    1. Run `adb usb`