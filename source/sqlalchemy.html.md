---
title: SQLAlchemy
category: Libraries
---

## Query objects

Get a query object as SQL formatted for a specific backend:

```python
from sqlalchemy.dialects.oracle import dialect as OracleDialect
compiled_query = my_query.compile(dialect=OracleDialect(use_ansi=False))  
sql_string = str(compiled_query)
sql_params = compiled_query.params
```
