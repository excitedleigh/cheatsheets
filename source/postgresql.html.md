---
title: PostgreSQL
category: Databases
---
List all databases/tables:

```sql
\l
\t
```

Describe a table:

```sql
\d tablename
```

Create a user and grant them access to an entire DB:

```sql
CREATE USER user WITH PASSWORD password;
GRANT ALL ON DATABASE db TO user;
```

## pg_restore

- `-c`,`--clean` Drop database objects before recreating.
    - `--if-exists`
- `-C`,`--create` Create the database before restoring.
    - `-Cc` drops and recreates the entire database.
- `-d`,`--dbname` Database to restore to.
- `-e`,`--exit-on-error`
- `-O`,`--no-owner` Don't set ownership of objects.
- `-x`,`--no-acl` Prevent restoring grant/revoke commands.
- `-1`,`--single-transaction`
    - Doesn't work with `-C`; use `-e` instead

## Fancy types

- [Range types](https://www.postgresql.org/docs/current/static/rangetypes.html) and [functions and operators for querying and manipulating them](https://www.postgresql.org/docs/current/static/functions-range.html)
