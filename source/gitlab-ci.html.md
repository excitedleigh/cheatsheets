---
title: GitLab CI
category: Tools
---

## Running builds locally

[Install gitlab-runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/bleeding-edge.md), and run `gitlab-runner exec docker my-job`.
