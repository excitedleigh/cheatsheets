---
title: JIRA
category: Platforms
---

## Useful Docs

- [JIRA Javadoc](https://docs.atlassian.com/jira/server/)
- [Atlassian Spring Scanner docs](https://bitbucket.org/atlassian/atlassian-spring-scanner)
- [Active Objects tutorial](https://developer.atlassian.com/docs/atlassian-platform-common-components/active-objects/getting-started-with-active-objects)

## Running the Atlassian PDK's embedded JIRA on PostgreSQL
1. Run atlas-run as normal, browse to http://localhost:2990/jira/plugins/servlet/applications/versions-licenses, then copy your JIRA testing license from that page.
1. Create a database called `jira`, then a user `jira` with password `jira` which has all permissions on that database. (Or whatever, just make sure you have a password; a blank one doesn't work.)
1. Replace the contents of `target/jira/home/dbconfig.xml` with the snippet below. (Source: [Atlassian Confluence](https://confluence.atlassian.com/adminjiraserver073/connecting-jira-applications-to-postgresql-861253040.html).)
2. Download the latest [PostgreSQL JDBC Driver](https://jdbc.postgresql.org/download.html) and drop the `.jar` file in `target/jira/webapp/WEB-INF/lib`.
5. Run atlas-run as normal, and enter the license key when prompted.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<jira-database-config>
  <name>defaultDS</name>
  <delegator-name>default</delegator-name>
  <database-type>postgres72</database-type>
  <schema-name>public</schema-name>
  <jdbc-datasource>
    <url>jdbc:postgresql://localhost:5432/jira</url>
    <driver-class>org.postgresql.Driver</driver-class>
    <username>jira</username>
    <password>jira</password>
    <pool-min-size>20</pool-min-size>
    <pool-max-size>20</pool-max-size>
    <pool-max-wait>30000</pool-max-wait>
    <pool-max-idle>20</pool-max-idle>
    <pool-remove-abandoned>true</pool-remove-abandoned>
    <pool-remove-abandoned-timeout>300</pool-remove-abandoned-timeout>

    <validation-query>select version();</validation-query>
    <min-evictable-idle-time-millis>60000</min-evictable-idle-time-millis>
    <time-between-eviction-runs-millis>300000</time-between-eviction-runs-millis>

    <pool-test-on-borrow>false</pool-test-on-borrow>
    <pool-test-while-idle>true</pool-test-while-idle>

  </jdbc-datasource>
</jira-database-config>
```

## Adding a LifecycleAware component

```diff
diff --git a/pom.xml b/pom.xml
index cda9192..7cf06aa 100644
--- a/pom.xml
+++ b/pom.xml
@@ -81,6 +81,18 @@
             <artifactId>gson</artifactId>
             <version>2.2.2-atlassian-1</version>
         </dependency>
+        <dependency>
+            <groupId>com.atlassian.sal</groupId>
+            <artifactId>sal-api</artifactId>
+            <version>3.0.3</version> <!-- https://developer.atlassian.com/docs/atlassian-platform-common-components/shared-access-layer/sal-version-matrix -->
+            <scope>provided</scope> <!-- Uses the application's SAL instead of bundling it into the plugin. -->
+        </dependency>
+        <dependency>
+            <groupId>org.springframework</groupId>
+            <artifactId>spring-context</artifactId>
+            <version>4.3.7.RELEASE</version>
+            <scope>provided</scope>
+        </dependency>
 
         <!-- Uncomment to use TestKit in your project. Details at https://bitbucket.org/atlassian/jira-testkit -->
         <!-- You can read more about TestKit at https://developer.atlassian.com/display/JIRADEV/Plugin+Tutorial+-+Smarter+integration+testing+with+TestKit -->
diff --git a/src/main/java/au/com/cmv/impl/TestImpl.java b/src/main/java/au/com/cmv/impl/TestImpl.java
index 6d2c800..6e9a1b4 100644
--- a/src/main/java/au/com/cmv/impl/TestImpl.java
+++ b/src/main/java/au/com/cmv/impl/TestImpl.java
@@ -1,7 +1,19 @@
 package au.com.cmv.impl;
 
+import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
+import com.atlassian.sal.api.lifecycle.LifecycleAware;
+import org.springframework.stereotype.Component;
+
 /**
  * Created by abrenecki on 21/3/17.
  */
-public class TestImpl {
+@Component("testImpl")
+@ExportAsService
+public class TestImpl implements LifecycleAware {
+    public void onStart(){
+        System.out.println("OMGOMGOMG ONSTART");
+    }
+    public void onStop(){
+        System.out.println("OMGOMGOMG ONSTOP");
+    }
 }

```

## Using Atlassian Scheduler

First, ensure you depend on the correct version in `pom.xml`:

```xml
<dependency>
    <groupId>com.atlassian.scheduler</groupId>
    <artifactId>atlassian-scheduler-api</artifactId>
    <version>1.3</version>
    <scope>provided</scope>
</dependency>
```

To determine the version, look up the correct version of SAL for the version of JIRA you're using in the SAL compatibility matrix, then find [SAL's `pom.xml` on BitBucket](https://bitbucket.org/atlassian/atlassian-sal/src/master/pom.xml) for the appropriate version tag, then depend on the same version SAL does.

Once that's done it's pretty simple:

```java
package com.example;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.*;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("lifecycleListener")
@ExportAsService
public class LifecycleListener implements LifecycleAware {
    private SchedulerService schedulerService;
    private final JobRunnerKey KEY = JobRunnerKey.of("com.example.syncJob");
    private final JobId ID = JobId.of("com.example.syncJob");

    public void onStart() {
        schedulerService = ComponentLocator.getComponent(SchedulerService.class);
        System.out.println("schedulerService");
        System.out.println(schedulerService);

        schedulerService.registerJobRunner(KEY, new JobRunner() {
            public JobRunnerResponse runJob(JobRunnerRequest jobRunnerRequest) {
                System.out.println("INTERVAL THINGY");
                return JobRunnerResponse.success();
            }
        });

        JobConfig jobConfig = JobConfig
                .forJobRunnerKey(KEY)
                .withSchedule(Schedule.forInterval(20000L, new Date()));
        try {
            schedulerService.scheduleJob(ID, jobConfig);
        } catch (SchedulerServiceException e) {
            e.printStackTrace();
        }
    }

    public void onStop() {
        schedulerService.unregisterJobRunner(KEY);
    }
}
```