---
title: reStructuredText
category: Markup
---

See also [Quick reStructuredText](http://docutils.sourceforge.net/docs/user/rst/quickref.html)

## Linking

```restructuredtext
Some text with a link_,
a `link with spaces`_,
an `inline link <https://example.org>`_,
and an `aliased link <alias_>`_.

.. _link: https://example.com
.. _link with spaces: https://example.net
.. _alias: https://python.org
```

## Cross-referencing

```restructuredtext
.. _some-label:

A section to cross-reference
----------------------------

See :ref:`some-label`.
```

## Sphinx

See also [Sphinx Markup Constructs](http://sphinx-doc.org/markup/)

### Autodoc

```restructuredtext
.. automodule:: abc.def.xyz

    .. autoclass:: foo

:class:`~abc.def.xyz.foo`
```

```python
#: Sphinx will display this text
THE_CONSTANT = 1

OTHER_CONSTANT = 2
"""Sphinx will display this text"""
```

```restructuredtext
.. autodata:: my.package.THE_CONSTANT
.. autodata:: my.package.OTHER_CONSTANT
```
