---
title: Sublime Text 3
category: Editors
---

## Panel Shortcuts

| Prefix | Keybinding | Panel |
|--------|------------|-------|
| none | Ctrl-P | Files in project |
| `@` | Ctrl-R | Symbols in file |
| n/a | Ctrl-Shift-R | Symbols in project |
| `>` | Ctrl-Shift-P | Command |
| `:` | Ctrl-G | Go to line |
