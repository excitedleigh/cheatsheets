---
title: Redux
category: Frameworks
---

## Boilerplate

```sh
yarn add react-redux redux remote-redux-devtools redux-thunk
```

### state/actions.js

```js
export const FOO = 'myapp.FOO'
```

### state/reducers.js

```js
import { combineReducers } from 'redux'
import * as actions from './actions'

function foo(state = {}, action){
  switch(action.type){
    case actions.FOO:
      return {...state, bar: 'baz'}
    default:
      return state
  }
}

export default combineReducers({foo})
```

### state/index.js

```js
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'remote-redux-devtools'
import reducer from './reducers'

let store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))
export default store
```

### containers/MyContainer.js

```jsx
import React, { Component } from 'react'
import { connect } from 'react-redux'

class MyContainer extends Component {
  render() {
    return []
  }
}

export default connect(
  (state, ownProps) => ({}),
  (dispatch, ownProps) => ({}),
)(MyContainer)
```

### index.js

```jsx
import store from './state'
import React, { Component } from 'react'
import { AppRegistry } from 'react-native'
import ROList from './containers/ROList'
import { Provider } from 'react-redux'

export default class DrivewayApp extends Component {
  render() {
    return <Provider store={store}>
      <ROList />
    </Provider>
  }
}

AppRegistry.registerComponent('DrivewayApp', () => DrivewayApp)
```