---
title: Django
category: Frameworks
---

Documentation links: 
[email](https://docs.djangoproject.com/en/1.8/topics/email/) 
[timezone](https://docs.djangoproject.com/en/1.9/ref/utils/#module-django.utils.timezone)

## Libraries

- [django-braces](http://django-braces.readthedocs.io/en/latest/) for [StaticContextMixin](http://django-braces.readthedocs.io/en/latest/other.html#staticcontextmixin) (although it's not necessary in Django 2.0+ due to [ContextMixin.extra_context](https://docs.djangoproject.com/en/2.0/ref/class-based-views/mixins-simple/#django.views.generic.base.ContextMixin.extra_context)

## Imports

```python
from django.core.mail import send_mail
from django.core.urlresolvers import reverse, reverse_lazy
from django.db.models import Q, F
from django.db.transaction import atomic
from django.template.loader import render_to_string
from django.utils.functional import memoize, cached_property
from django.utils.timezone import now, make_aware # use instead of datetime.datetime.now()
```

## Configuration

Configure Django to send a bunch of headers that activate extra security
precautions on modern clients, including Strict Transport Security and
Content Security Policy. Requires the third party `django-csp` package.

```python
MIDDLEWARE_CLASSES += (
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'csp.middleware.CSPMiddleware',
)

SECURE_SSL_REDIRECT = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_SECONDS = 60 * 60 * 24 * 365

# Example only, adjust as appropriate
CSP_DEFAULT_SRC = ["'self'"]
CSP_STYLE_SRC = CSP_DEFAULT_SRC + ["https://fonts.googleapis.com"]
CSP_FONT_SRC = CSP_DEFAULT_SRC + ["https://fonts.gstatic.com"]
CSP_MEDIA_SRC = ["'none'"]
CSP_OBJECT_SRC = ["'none'"]
CSP_FRAME_SRC = ["'none'"]
```

## Management commands

```shell
django-admin changepassword [username]
```

## Views

Customizing a model about to be saved in a CBV:

```python
class MyView(CreateView):
    def form_valid(self, form):
        form.instance.x = y
        return super().form_valid(form)
```

## Forms

ModelForms where some attributes of the model are set by the form, but others are set from other data (e.g. URL parameters, currently logged-in user, etc):

```python
class MyForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

    def clean(self):
        # Fill in the fields before we call clean(), because clean() calls the
        # model's full_clean() method
        self.instance.user = self.user
        super().clean()
```

## Models

```python
from django.db import models

class MyQuerySet(models.QuerySet):
    pass

class MyModel(models.Model):
    objects = MyQuerySet.as_manager()
    # When you need to use another custom manager class, this can work too
    # Note that it might not if MyCustomManager naively overwrites get_queryset
    other = MyCustomManager.from_queryset(MyQuerySet)()

    def clean(self):
        raise ValidationError({'field_name': 'message'})
```

Alternately, if the manager needs to be used for related fields, it's necessary to do this:

```python
class MyQuerySet(models.QuerySet):
    pass

class MyManager(models.Manager.from_queryset(MyQuerySet)):
    use_for_related_fields = True

class MyModel(models.Model):
    objects = MyManager()
```

## Permissions

Default permissions: `app.{add,change,delete}_modelname`

## Testing

[RequestFactory](https://docs.djangoproject.com/en/dev/topics/testing/advanced/#django.test.RequestFactory):

```python
from django.test import RequestFactory
factory = RequestFactory()
request = factory.get('/a/b')
response = view_to_test(request)
```

### Testing the Core

https://docs.djangoproject.com/en/1.8/internals/contributing/writing-code/unit-tests/

## Date formatting in templates

Sorted by value type. For a list sorted by character, go to [the official Django docs](https://docs.djangoproject.com/en/1.10/ref/templates/builtins/#date).

| Value | Character | Format | Example |
| ----- | --------- | ------ | ------- |
| **Entire Date** | `c` | ISO 8601 | 2008-01-02T10:30:00.000123+02:00 |
|  | `r` | RFC 5322 | Thu, 21 Dec 2000 16:01:07 +0200 |
|  | `U` | Unix time | 1490329139 |
| **Year** | `L` | Whether a leap year | True or False |
|  | `y` | 2 digits | 17 |
|  | `Y` | 4 digits | 2017 |
| **Week Year** | `o` | 4 digits | 1999 |
| **Month** | `b` | Text, 3 letters, lowercase | jan |
|  | `E` | Locale-specific alternative | listopada (PL locale) |
|  | `F` | Text, long | January |
|  | `m` | Numeric, leading zero | 01 |
|  | `M` | Text, 3 letters | Jan |
|  | `n` | Numeric | 1 |
|  | `N` | Text, abbreviated, AP style | Jan., March |
| **Week** | `W` | Numeric | 1, 53 |
| **Day of Year** | `z` | | 298 |
| **Day of Month** | `d` | Leading zero | 01 |
|  | `j` | No leading zero | 1 |
|  | `S` | EN ordinal suffix | st, nd, rd, th |
| **Total Days In Month** | `t` | | 28, 31 |
| **Day of Week** | `D` | Text, 3 letters | Fri |
|  | `l` | Text, long | Friday |
|  | `w` | Numeric | 0 (Sun), 6 (Sat) |
| **Entire Time** | `f` | 12-hour hours:minutes, minutes omitted if zero | 1, 1:30 |
|  | `P` | 12-hour hours:minutes, a.m. / p.m. | 1 a.m., 1:30 p.m., midnight, noon |
| **Hour** | `g` | 12-hour | 1 |
|  | `G` | 24-hour | 1, 13 |
|  | `h` | 12-hour, leading zero | 01 |
|  | `H` | 24-hour, leading zero | 01, 13 |
| **Minute** | `i` | | 05 |
| **Second** | `s` | | 05 |
| **Microsecond** | `u` | | 958305 |
| **AM/PM** | `a` | Lowercase, AP style | a.m. |
|  | `A` | Uppercase | AM |
| **Timezone** | `e` | Name | GMT, -500, US/Eastern, empty string |
|  | `I` | Whether DST is in effect | 1 or 0 |
|  | `O` | UTC offset, HHMM format | +0200 |
|  | `T` | "Time zone of this machine" | EST, MDT |
|  | `Z` | UTC offset in seconds | 34200 |
