---
name: XML
category: Serialisation Formats
---

## Schema Validation

[RELAX NG](http://relaxng.org/tutorial-20011203.html) seems to be both complete and concise. It supports [datatyping](http://relaxng.org/compact-tutorial-20030326.html#id2814737), but lxml doesn't actually parse the types.

lxml (and probably most other libraries) support the XML Schema datatypes, so it's easiest to just specify that in the outermost element: `<element name="foo" xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">`.

## XPath

- Get text of an element using its absolute path: `//event/lane/id/text()`
    - lxml: `doc.xpath('//event/lane/id/text()')` (returns an array with 0 items if the element doesn't have inner text or 1 if it does)

## lxml

Example of parsing and validating:

```python
import pkg_resources

from lxml import etree

schema_str = pkg_resources.resource_string(
    'numberplate', 'apps/numberplate/data/event.rng')
schema = etree.RelaxNG(etree.fromstring(schema_str))

def consume(xml_str):
    try:
        doc = etree.fromstring(xml_str)
    except etree.XMLSyntaxError as e:
        return HttpResponseBadRequest(str(e))
    try:
        schema.assertValid(doc)
    except etree.DocumentInvalid as e:
        return HttpResponse(str(e), status=422)
```