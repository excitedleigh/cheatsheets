---
title: Django CMS
category: Frameworks
---

## Template Contexts

### Page

- Title: `request.current_page.get_title`

## Plugins

### Plugin (in `app/cms_plugins.py`)

```python
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from djangocms_link.models import Link


class MyPlugin(CMSPluginBase):
    name = "User-Friendly Name"
    module = "Category Header in Add Plugin UI"
    model = CMSPlugin # override me!
    render_template = 'path/to/frontend/template.html'
    allow_children = True
    require_parent = True
    parent_classes = ['ListOf', 'AllowedParent', 'PluginNames']
    child_classes = ['ListOf', 'AllowedChild', 'PluginNames']
    render_plugin = True # set to False if the parent plugin renders for you
                         # must be set to False if render_template not set


plugin_pool.register_plugin(MyPlugin)
```