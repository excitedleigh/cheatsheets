---
title: Emacs
category: Editors
---

## Core Keybindings

- `C-x C-e` - Evaluate the elisp expression preceding the cursor
- `M-x M-c` - Quit
- `C-h f function-name` - Help for a function
- `C-h k key-sequence` - Find the function a key sequence is bound to

## Spacemacs

- `SPC f e d` - Edit `.spacemacs`
- `SPC f e R` - Save `.spacemacs`
- `SPC h SPC` - Spacemacs help and layer list

### Buffers

- `SPC b d` - Kill buffer

### Windows

- `SPC w c` - Delete window

### Git

- `SPC g s` - Git status
