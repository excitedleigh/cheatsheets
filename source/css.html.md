---
category: Languages
title: CSS
---

## Centre content horizontally and vertically

Centres block-level children horizontally and vertically.

```css
.parent {
    display: flex;
    align-items: center;
    justify-content: center;
}
/* The following can be useful if the parent is likely to occupy more than one
   screen-height of vertical space. It will move the centred content up or
   down to ensure it's never within 20px of the viewport edge. */
.parent > div {
    position: sticky;
    top: 20px;
    bottom: 20px;
}
```

```html
<div class='parent'>
    <div>
        Centred content
    </div>
</div>
```

## Overlay content over a parent element

```css
.parent {
    position: relative;
}
.inFlight {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 1;
    background-color: rgba(255, 255, 255, 50%);
}
```

```html
<div class='parent'>
    <!-- some other content -->
    <div className='inFlight'>
        Loading...
    </div>
</div>
```