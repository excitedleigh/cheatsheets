---
title: Rust
category: Languages
---

## Fighting the Borrow Checker

### When returning a Map or Filter in a method, remember the closure will outlive the function body (and potentially any arguments), so you need to move any data you require into it.

```rust
// Wrong
fn return_iter(&self){
    some_iter()
        .filter(|x| x > self.y)
}

// Right
fn return_iter(&self){
    let y = self.y.clone();
    some_iter()
        .filter(move |x| x > y)
}
```