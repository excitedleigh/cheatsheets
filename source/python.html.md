---
title: Python
category: Languages
---

## Useful Libraries

- [attrs](http://www.attrs.org/en/stable/) for classes without boilerplate
- [begins](http://begins.readthedocs.io/en/latest/index.html) for argument parsing
- [structlog](https://structlog.readthedocs.io/en/stable/) for structured logging
- [urlobject](http://zacharyvoase.github.io/urlobject/) for manipulating URLs

## Useful Parts of the Standard Library

- [pathlib](https://docs.python.org/3/library/pathlib.html) for manipulating filesystem paths

## Useful Tools

- [safety](https://pyup.io/safety/) for checking your dependencies for security vulnerabilities

## Builtins

```python
range(5, 10) # -> [5, 6, 7, 8, 9] (py2); range object over same (py3)
```
## String Formatting

```
"{" [field_name] ["!" conversion] [":" format_spec] "}"
```

- `field_name`: Object to format and present here. Default is the next positional argument.
- `conversion`: `r` (`repr()`) or `s` (`str()`) (default is to call `value.__format__()`)

```
[ [fill] align] [sign] ["#"] ["0"] [width] [","] ["." precision] [type]
```

- `fill`: character to fill empty space with when using `align`. Defaults to space.
- `align`: Align to the left (`<`), right (`>`), centre (`^`), or before digits (`=`, numeric only).
- `sign`: Numeric only. Print a sign for negative numbers (`-`, default), positive and negative (`+`), or space for positive and sign for negative (` `).
- `#`: Integer only, bin/oct/hex only. If present, use preceding `0b`/`0o`/`0x`.
- `0`: Numeric only. Sign-aware zero padding. Equivalent to `fill='0', align='='`.
- `width`: Minimum field width.
- `,`: Print a `,` as a thousands separator.
- `precision`:
    - Not allowed for integers.
    - Digits after decimals, for type `f`/`F`.
    - Digits all up, for type `g`/`G`.
    - Maximum field size, for non-numbers.
- `type`:
    - For strings: `s`, which can be omitted.
    - For integers:
        - `b`: Binary.
        - `c`: Unicode character at this code point.
        - `d`: Decimal, default.
        - `o`: Octal.
        - `x`/`X`: Hexadecimal, lowercase/uppercase a-f.
        - `n`: Decimal, with locale-appropriate separators.
    - For floats (or integers, which are passed through `float()`):
        - `e`/`E`: Exponent notation, lowercase/uppercase e. Default precision is 6.
        - `f`/`F`: Fixed point. Default precision is 6.
        - `g`/`G`: General. `f` or `e`/`E` depending on magnitude, but with trailing zeros removed. Default `g`.
        - `n`: Same as `g` but with locale-appropriate separators.
        - `%`: Like `f`, but multiplied by 100 and with a `%` character appended.

## Argparse

```python
parser = argparse.ArgumentParser()
parser.add_argument('infile', nargs='?', type=argparse.FileType('r'),
                 default=sys.stdin)
parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'),
                 default=sys.stdout)
args = parser.parse_args()
args.infile.read()
```

### `add_argument` args

- Positional arguments: name and/or flag(s)
- `action`: What to do with the argument.
    - `'store'`: Store the value passed in, coerced with `type`.
    - `'store_const'`, `'store_true'`, `'store_false'`: Store the value passed in as the **`const` arg**, `True` or `False` respectively.
    - `'append'`: Store a list of values passed in.
    - `'append_const'`: Store a list of values from the **`const` arg** (eg when multiple args have the same `dest`).
    - `'count'`: Store the number of times the arg occurs.
    - `'help'`, `'version'`: Print help or version info and exit.
- `nargs`: How many values should be expected.
    - An integer `n`: Expect exactly `n` values, e.g. `--foo a b` gives `['a', 'b']`.
    - `'?'`: Optional - consume one argument if present.
    - `'*'`, `'+'`: Accumulate as many as possible into a list. `'+'` requires at least one.
    - `argparse.REMAINDER`: Anything not already consumed.
- `default`: Value to use if not present. 
- `type`: Callable to convert value to desired type.
    - `argparse.FileType(mode, bufsize, encoding, errors)`: Accept a file path.
- `choices`: Collection to restrict possible values to.
- `required`: Mark an otherwise-optional `--param` as required.
- `help`: Help string for this arg.
- `metavar`: Placeholder for this arg in help string.
- `dest`: Property to assign this arg to in `parse_args()`.

## Packaging

### The standard way

```python
from setuptools import find_packages, setup

setup(
    name='myproject',
    description='My awesome project',
    author='Adam Brenecki',
    author_email='adam@brenecki.id.au',
    license='Proprietary',
    setup_requires=["setuptools_scm>=1.11.1"],
    use_scm_version=True,
    packages=find_packages(),
    # Need to include py_modules if using single-file modules, since
    # find_packages doesn't find them
    # py_modules=['mymodule'],
    include_package_data=True,
    install_requires=[
        'dependency1',
        'dependency2',
    ],
    entry_points={
        'console_scripts': ['my-cli-app=my.module.path:my_callable'],
    }
)
```

### With `pbr`

- Anything that should be distributed with your app (eg Django staticfiles and templates) *must* go inside your package directory.
- Anything else should go outside, but can also be excluded by `MANIFEST.in` otherwise.
- Requirements go in `requirements.txt`.

#### setup.cfg

```ini
[metadata]
name = mypkg
author = Adam Brenecki
author-email = adam@brenecki.id.au
summary = My awesome package
description-file = README.md
license = MIT
[files]
packages =
    mypkg
```

#### setup.py

```python
#!/usr/bin/env python

from setuptools import setup

setup(
    setup_requires=['pbr>=1.8,<1.9', 'setuptools>=17.1'],
    pbr=True,
)
```

### MANIFEST.in

PBR and/or setuptools_git will include everything version controlled by default. Anything that's not
version controlled (eg compiled/minified static files) needs to be included, and
anything that's not needed in the package needs to be excluded, in
`MANIFEST.in`.

```
recursive-include mypkg/built-static *
prune mypkg/my-sass
```

## Tricks

### Writing a CLI app that connects to a thing on a remote server

In this situation, it's useful to be able to tunnel through SSH. Here's an example of doing that using sshtunnel:

```python
import logging
import re

import begin
from MySQLdb import connect
from sshtunnel import SSHTunnelForwarder
from urlobject import URLObject

logger = logging.getLogger(__name__)


class PossiblyTunneledMySQLConnection:
    def __init__(self, dealerpro_url, ssh_url):
        self.dp = URLObject(dealerpro_url)
        if ssh_url is not None:
            self.ssh = URLObject(ssh_url)
        else:
            self.ssh = None

    def __enter__(self):
        if self.ssh is None:
            self.forwarder = None
            self.mysql = connect(
                host=self.dp.hostname,
                user=self.dp.username,
                passwd=self.dp.password,
                db=self.dp.path[1:],
                port=self.dp.port or 3306,
            )
        else:
            self.forwarder = SSHTunnelForwarder(
                (self.ssh.hostname, self.ssh.port) if self.ssh.port else self.ssh.hostname,
                ssh_username=self.ssh.username,
                ssh_password=self.ssh.password,
                remote_bind_address=(self.dp.hostname, self.dp.port or 3306),
                ssh_pkey='/Users/abrenecki/.ssh/id_rsa',
            )
            self.forwarder.start()
            self.mysql = connect(
                host=self.forwarder.local_bind_host,
                user=self.dp.username,
                passwd=self.dp.password,
                db=self.dp.path[1:],
                port=self.forwarder.local_bind_port,
            )
        return self.mysql

    def __exit__(self, type, value, tb):
        if self.forwarder is not None:
            self.forwarder.stop()
        self.mysql.close()


@begin.start
@begin.tracebacks
@begin.logging
def main(
    mysql_url: "URL to the DealerPro MySQL database",
    tunnel: "URL of a SSH host to tunnel through" = None,
):
    count = int(count)
    with PossiblyTunneledMySQLConnection(dealerpro_url, tunnel) as conn:
        pass # do your thing here
```

### Prompting for and storing a password

```python
import getpass

import begin
import keyring
import requests

@begin.start
def main(  # noqa
    username: "Username to log in with" = None,
    password: "Password to log in with" = None,
    service_url: "Alternate URL to contact the service" = "https://example.com",
):
    password_from_keyring = False
    if username is None:
        username = getpass.getuser()
    if password is None:
        password = keyring.get_password(service_url, username)
        password_from_keyring = True
    if password is None:
        password = getpass.getpass("{1} @ {0} password: ".format(
            service_url, username))
        keyring.set_password(service_url, username, password)
    try:
        pass  # do stuff
    except requests.HTTPError as e:
        if e.response.status_code == 403:
            print("Authentictation details were incorrect.")
            if password_from_keyring:
                keyring.delete_password(service_url, username)
            exit(1)
        else:
            raise
```
