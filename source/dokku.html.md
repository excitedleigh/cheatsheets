---
title: Dokku
category: Operations
---

## Setting up Dokku

Create a new Ubuntu LTS machine, make an A record from `apps.example.com` to it, and a CNAME record from `*.apps.example.com` to `apps.example.com`.

```sh
# Set up unattended-upgrades
apt install unattended-upgrades mosh update-notifier-common
sudo dpkg-reconfigure --priority=low unattended-upgrades
echo 'Unattended-Upgrade::Automatic-Reboot "true";' > /etc/apt/apt.conf.d/21auto-upgrades-reboot

# Set up firewall
ufw allow ssh
ufw allow mosh
ufw allow 80/tcp
ufw allow 443/tcp
ufw enable

# Install dokku
wget https://raw.githubusercontent.com/dokku/dokku/v0.11.2/bootstrap.sh
sudo DOKKU_TAG=v0.11.2 bash bootstrap.sh
# navigate to web ui and finish setup
sudo dokku plugin:install https://github.com/dokku/dokku-postgres.git postgres
dokku plugin:install https://github.com/dokku/dokku-redis.git redis
dokku plugin:install https://github.com/dokku/dokku-letsencrypt.git
dokku config:set --global --no-restart DOKKU_LETSENCRYPT_EMAIL=adam@brenecki.id.au
dokku letsencrypt:cron-job --add
```

## Deploying a new app

```
dokku apps:create myapp
dokku postgres:create myapp-db
dokku postgres:link myapp-db myapp
dokku redis:create myapp-cache
dokku redis:link myapp-cache myapp
dokku config:set KEY1=VAL1 KEY2=VAL2
dokku proxy:ports-clear myapp
dokku proxy:ports-add http:80:8000
dokku domains:add myapp myapp.com
git remote add dokku dokku@apps.a93.me:myapp
git push dokku master
dokku letsencrypt myapp
```

## Deploying Piwik

```
dokku apps:create piwik
dokku mariadb:create piwik-db
dokku mariadb:link piwik-db piwik
docker pull piwik
docker tag piwik dokku/piwik:v1
dokku tags:deploy piwik v1
dokku letsencrypt piwik
```