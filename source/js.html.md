---
title: JavaScript
category: Languages
---

## Resources

- [Exploring ES6 book](http://exploringjs.com/es6/)

## Promises

- If you have a `thing` that might be a `T` or a `Promise<T>`, call `Promise.resolve(thing)`; this returns a `Promise<T>` that resolves immediately if `thing` is a `T`, or when `thing` resolves if `thing` is a `Promise<T>`.

## Snippets

### Get Django CSRF token from cookie

```js
decodeURIComponent(/(?:^|;)\s*csrftoken=([^;]+)/.exec(document.cookie)[1])
```

### DOMContentLoaded

```js
// Longer version is resilient to DOMContentLoaded firing between the readyState
// test and the call to addEventListener - not sure if necessary or not
function(c){var d=document,f,b=function(){f||c();f=1};d.addEventListener('DOMContentLoaded',b);/c/.test(d.readyState)&&b()}
// Shorter, more conventional version
function(c){/c/.test(document.readyState)&&c()||document.addEventListener('DOMContentLoaded',c)}
// yes, in this case separating out `document` into a `d` var makes it longer
```

### Script loader

```js
// Inserts before current script; can't be used asynchronously
function(u,c){var d=document,r=d.currentScript,s=d.createElement('script');s.src=u;s.async=true;c&&(s.onload=c);r.parentNode.insertBefore(s,r)}
// Inserts at end of <head>
function(u,c){var d=document,s=d.createElement('script');s.src=u;s.async=true;c&&(s.onload=c);d.head.appendChild(s)}
```

### Random ID generator

```js
(n='id')=>n+Math.random().toString(36).slice(2)
// ES5 version
function id(n){return (n||'id')+Math.random().toString(36).slice(2)}
```

### Sequential ID generator

```js
// Factory version
(n)=>{let i=0;return ()=>n+i++}
// Factory version, ES5
function idFactory(n){let i=0;return function(){return n+i++}}
// Global counter version
(()=>{let i=0;return (n='id')=>n+i++})()
// Global counter version, ES Module
let i=0;export default (n='id')=>n+i++
// Global counter version, ES5
function(){let i=0;return function(n){return (n||'id')+i++}}
```