---
title: HTML
category: Languages
---

## Video

Looping silent auto-playing videos (e.g. replacements for GIFs) [that work in Mobile Safari](https://webkit.org/blog/6784/new-video-policies-for-ios/):

```html
<video autoplay loop muted playsinline>
  <source src="image.mp4">
  <source src="image.webm" onerror="fallback(parentNode)">
  <img src="image.gif">
</video>
```