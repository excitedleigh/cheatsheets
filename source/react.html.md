---
title: React
category: Frameworks
---

## Redux

### Connect Redux developer tools

```typescript
const reducer = combineReducers<State>({thing, otherThing})
let middleware = applyMiddleware(thunkMiddleware)

// Hook in Redux dev tools if present
declare var process : any
if (process.env.NODE_ENV != "production" && typeof window['devToolsExtension'] != "undefined"){
  middleware = compose(middleware, window['devToolsExtension']())
}

export default createStore(reducer, middleware)
```