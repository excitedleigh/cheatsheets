---
title: Git
category: Tools
---

## Staging and Committing

```shell
git reset my_file # unstage my_file
git rm --cached my_file # delete my_file from the stage, leave it in the working copy
git add --patch # interactively add chunks of files
git reset --soft HEAD~ # undo last commit
```

## Branches

```shell
git branch -d branch-to-delete
git branch -f branch-to-move ref-to-move-to
```

### Rebasing

Basic usage: Cut the branch from `snip-from` to `snip-to`, and graft it onto `graft-point`. If not specified, `snip-to` is the current branch, `graft-point` is the current branch's upstream, and `snip-from` is their common ancestor.

```sh
git rebase [--onto graft-point] snip-from snip-to
```

Replay `feature-branch` on top of master:

```sh
git checkout feature-branch
git rebase master
# or
git rebase master feature-branch
```

Replay the difference between `feature-branch`, starting from the point where it diverges from `master`, on top of `v1.2.3`

(Note: I'm not sure if the final `feature-branch` is required if you're already checked out on it)

```sh
git checkout feature-branch
git rebase --onto v1.2.3 master
```

Move some commits that were accidentally committed on `feature-branch-a` onto a new branch called `feature-branch-b`. (Replace `origin/feature-branch-a` with the ref of the latest commit of `feature-branch-a` that you __don't__ want to copy to the new branch.)

```sh
# Create a new branch for your misplaced commits
git checkout -b feature-branch-b
# Put feature-branch-a back to where it was before the misplaced commits (replace origin/... as appropriate)
git branch -f feature-branch-a origin/feature-branch-a
# Snip at feature-branch-a, graft to master
git rebase --onto master feature-branch-a
```

Move a single commit from a feature branch onto master:

```sh
git rebase --onto HEAD 0a10113^ 0a10113
```

## Easily Squashing a Branch

If you have a whole bunch of changes on `branchA` that you want to squash onto `branchB`:

```shell
git checkout branchB
git checkout -p branchA # this command will let you review changes
git commit
```

## Stashes

```shell
git stash create -u [message] # -u = include untracked files
git stash apply [stash] # apply a stash, but leave it in the list
git stash pop [stash]   # equivalent to git stash apply && git stash drop
git stash show -p [stash] | git apply -R # unapply a stash
git stash branch [stash] # turn a stash into a branch
```

[in Pro Git](https://git-scm.com/book/no-nb/v1/Git-Tools-Stashing)

## Finding Changes

```shell
git branch --contains commit-ref
git log --all --grep pattern # Search all commit messages for pattern
git log -L 52,54:my_file.py # Show only commits that change these lines of this
                            # file, with diffs
```

## Moving a Repo

Moving a bare repo (or a folder thereof) on to a remote:

```fish
for x in *
  git --git-dir=$x push --mirror http://gitlab.cmvhodom.cmv/githost/$x.git
end
```

Bundling a repo into a single file, which can then be treated like a Git remote:

```shell
git bundle create my_repo.bundle --all
```
