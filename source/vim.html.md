---
title: Vim
category: Editors
---

## Movement

```
fa / Fa         Jump to the next occurence of 'a'
%               Jump between opening/closing bracket
```

## Filtering

```
:%!isort -       Run buffer / selected text through `isort -`
```

## Navigating Diffs

```
do              Pull (obtain) content from other side of the diff
dp              Push content into the other side of the diff
```

## Splits

```
:new, :vnew    Open a new buffer in a split / vsplit
```
