---
category: Languages
title: TypeScript
---

# Typing a React Higher-Order Component

In this example, say you want to be able to wrap components with props `foo`, `bar`, and some other props, returning a component that takes `foo` and the other props (with `bar` being derived).

```typescript
function wrapper<P extends {foo: number}>(
  InnerComponent: React.ComponentType<P & {bar: string}>,
): React.ComponentType<P> {
  class OuterComponent extends React.Component<P, {bar: string}>{
    // TODO: calculate bar and store in this.state
    render(){
      return <InnerComponent {...this.props} bar={this.state.bar} />
    }
  }
  return OuterComponent
}
```

TypeScript needs to be given a bit more help at the call site, though, otherwise it will put `bar` in the output type:

```typescript
const WrappedClass = wrapper<{foo: number, baz: string}>(Class)
```

Once [support for an `Omit<T, K>` type](https://github.com/Microsoft/TypeScript/issues/12215) lands in TypeScript, you could instead use the following which removes the need for the explicit type at the call site. (Note that none of the workarounds in that issue work for generics.)

```typescript
function wrapper<P extends {foo: number, bar: string}>(
  InnerComponent: React.ComponentType<P>,
): React.ComponentType<Omit<P, 'bar'>> {
  class OuterComponent extends React.Component<Omit<P, 'bar'>, {bar: string}>{
    // TODO: calculate bar and store in this.state
    render(){
      return <InnerComponent {...this.props} bar={this.state.bar} />
    }
  }
  return OuterComponent
}
```