---
title: systemd
category: System Utilities
---

[List of all systemd directives](https://www.freedesktop.org/software/systemd/man/systemd.directives.html).

## Service files

Saved in `/etc/systemd/system/myservice.service`

```ini
[Unit]
Description=Description of the service
After=mydependency.service

[Service]
ExecStart=/usr/bin/my-service --args --here
User=myuser
WorkingDirectory=/my/working/directory
Restart=always
Environment=KEY1=VALUE1
Environment=KEY2=VALUE2

[Install]
WantedBy=multi-user.target
```

- To enable and immediately start: `sudo systemctl enable --now myservice`
- To view output: `journalctl -u myservice`

## Mount units

The systemd man pages suggest using `/etc/fstab` instead, but here's one anyway. Note that the unit must be named according to the mount path with dashes instead of slashes, so in this case `mnt-marketing.mount`.

```ini
[Unit]
Description=Marketing

[Mount]
What=//storage/Marketing
Where=/mnt/marketing
Type=cifs
Options=credentials=/opt/cifs-creds.txt,uid=adam,gid=adam

[Install]
WantedBy=multi-user.target
```

## Systemd tricks

- Starting/stopping multiple units in lockstep (a la groups in Supervisor): [Use the `Unit.BindTo` attribute](http://serverfault.com/questions/332399/is-there-a-way-to-control-two-instantiated-systemd-services-as-a-single-unit/332405#332405).
