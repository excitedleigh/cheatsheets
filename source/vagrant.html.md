---
title: Vagrant
category: Tools
---

## Troubleshooting

### VM won't boot

- Is virtualisation turned on in the computer's firmware (or in the hypervisor if running Vagrant from within a VM)?
- Add `config.vm.provider "virtualbox" do |vb|; vb.gui = true; end` to `Vagrantfile` and watch the VM boot up.
- `vagrant plugin install vagrant-vbguest` seems to help on CentOS images.
