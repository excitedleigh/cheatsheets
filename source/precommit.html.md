---
title: Pre-commit
category: Tools
---

Useful things to have in your `.pre-commit-config.yaml`.

After adding, run `pre-commit autoupdate` to update the SHAs.

## All file types

```yaml
- repo: https://github.com/pre-commit/pre-commit-hooks
  sha: master
  hooks:
  - id: check-added-large-files
  - id: check-case-conflict
  - id: check-merge-conflict
  - id: debug-statements
  - id: end-of-file-fixer
  - id: trailing-whitespace
- repo: https://github.com/Lucas-C/pre-commit-hooks
  sha: master
  hooks:
  - id: forbid-crlf
    exclude: \.csv$
  - id: forbid-tabs
    exclude: Makefile|\.bat$
```

## Python

```yaml
# this is the same repo as above, so append this bit to that
- repo: https://github.com/pre-commit/pre-commit-hooks
  sha: master
  hooks:
  - id: autopep8-wrapper
    exclude: migrations\/[^/]*\.py$|settings\/[^/]*\.py$
- repo: git://github.com/FalconSocial/pre-commit-python-sorter
  sha: master
  hooks:
  - id: python-import-sorter
    exclude: migrations\/[^/]*\.py$
- repo: git://github.com/guykisel/prospector-mirror
  sha: master
  hooks:
  - id: prospector
    exclude: migrations\/[^/]*\.py$|settings\/[^/]*\.py$
```
