---
title: React Native
category: Frameworks
---

## Releasing

1. Set up the project with [react-native-version](https://www.npmjs.com/package/react-native-version), if you haven't already.
2. `npm version x.y.z` (this will create a new Git commit and tag).
3. Follow the instructions for signing keys [in the React Native docs](https://facebook.github.io/react-native/docs/signed-apk-android.html), if you haven't already.
4. `cd android && ./gradlew assembleRelease && adb install app/build/outputs/apk/app-release.apk`

## ListView

Example of wrapping ListView to be a bit easier to manage:

```jsx
import React, { Component } from "react"
import { StyleSheet, ListView, ScrollView } from "react-native"

function defaultRowHasChanged(r1, r2) {
  return r1 !== r2
}

export default class ListViewWrapper extends Component {
  constructor(props) {
    super(props)
    const ds = new ListView.DataSource({
      rowHasChanged: props.rowHasChanged || defaultRowHasChanged,
    })
    this.state = {
      dataSource: ds.cloneWithRows(this.props.rows),
    }
  }
  componentWillReceiveProps(newProps) {
    const newRowHasChanged = newProps.rowHasChanged || defaultRowHasChanged
    var ds
    if (newRowHasChanged !== this.props.rowHasChanged) {
      ds = new ListView.DataSource({ rowHasChanged: newRowHasChanged })
    } else {
      ds = this.state.dataSource
    }
    this.setState({ dataSource: ds.cloneWithRows(newProps.rows) })
  }
  render() {
    // Pass all props except rowHasChanged and rows through to ListView
    const { rowHasChanged: _a, rows: _b, ...listViewProps } = this.props
    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this.props.renderRow}
        {...listViewProps}
      />
    )
  }
}
```