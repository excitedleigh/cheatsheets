---
title: CLI tools
category: System Utilities
---

## Unix utils

### cut

```sh
echo "a,b,c" | cut -d, -f2 # => "b"
```

## wget

```bash
# Archive an entire website, to be viewed somewhere else:
wget -mpkKE http://example.com/
# Archive an entire website, to be uploaded as a static site in the original location:
# (ie don't rewrite URLs)
wget -mpE https://example.com/
```

## HTTPie

```bash
http \
    -pHBhb \ # print REQUEST, response headers/body (-v is a shortcut for this)
    --auth username:password \ # HTTP basic authentication
    -f \ # send as form-encoded data, -j or omit for JSON
    post \ # HTTP verb, omit for GET
    example.com/url/goes/here \
    HeaderKey:HeaderValue \
    FormKey=FormValue \
    FormKey@PathToFileToUpload \ # -f only
    QueryStringKey==QueryStringValue
```

## Package Managers

### Yum

```shell
yum provides xmlversion.h # Which package provides xmlversion.h?
```

## Date

`date +%FT%T%z` - Print the current datetime in ISO format. (`-Iseconds` works on Linux, but not BSD/OS X).

## Screen

- `screen -S sessionname` - Start a named session
- `screen -ls` - List running sessions
- `screen -r [sessionname]` - Rejoin a running session
- `C-a d` - Detach from session