---
title: Webpack
category: Tools
---

## `webpack.config.js`

### Minimal

`npm install --save-dev webpack`

```js
const resolve = require('path').resolve

module.exports = {
    entry:  './js',
    output: {
        path:     resolve('./path/to/static/build'),
        filename: 'bundle.js',
        publicPath: '/static/build/',
    },
}
```

### Sass


```js
var CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin')
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    // ...
    module: {
        loaders: [
            {
                test: /\.s?css$/,
                loader: ExtractTextPlugin.extract('style', 'css!sass'),
            }
        ]
    },
    plugins: [
        new CommonsChunkPlugin({children: true, async: true}),
        new ExtractTextPlugin("main.css"),
    ]
}
```

### ES6

_Note: References to `babel-polyfill` only necessary if using ES6 features that require it (eg generators, `Map`, `async`, `Promise`, etc - not new syntactic sugar only).

`npm install --save-dev babel-polyfill babel-loader babel-preset-es2015`

```js
module.exports = {
    entry:  ['babel-polyfill', './js'],
    // ...
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader?presets[]=es2015',
            },
        ],
    }
}
```

### ES6 + React

```js
webpack = require('webpack')

module.exports = {
    // ...
    module: {
        loaders: [
            {
                test: /\.jsx$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react'],
                    plugins: ['transform-class-properties',
                              'transform-decorators-legacy'],
                }
            },
        ]
    },
}
```

### ES6 + TypeScript

`npm install --save-dev typescript ts-loader`

```js
module.exports = {
    // ...
    loaders: [
        { test: /\.ts(x?)$/, loader: 'babel-loader?presets[]=es2015!ts-loader' },
    ],
}
```

### Conditionally including code in development

```js
webpack = require('webpack')

module.exports = {
    // ...
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
            }
        }),
    ]
}
```

## `package.json`

```json
{
  "scripts": {
    "watch": "webpack -d --watch",
    "production": "NODE_ENV=production webpack -p --bail"
  }
}
```
