---
title: Bash
category: System Utilities
---

## Detatch an already running process from the shell

```sh
^Z # pause it and return to the shell
bg # start it running again
disown %1 # detatch it from your shell process
```
