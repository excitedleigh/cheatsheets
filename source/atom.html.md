---
title: Atom
category: Editors
---

## Keybindings

### Fuzzy Things

- `Ctrl-P` - fuzzy file finder
- `Ctrl-Shift-P` - command pallete
- `Ctrl-R` - fuzzy symbol finder in file (provided by **goto**, **atom-ctags**)
- `Ctrl-Shift-R` - fuzzy symbol finder in project (provided by **goto**, **atom-ctags**)
- `Ctrl-G` - go to line
- `Ctrl-Alt-Down` - go to declaration (provided by **goto**, **atom-ctags**)
