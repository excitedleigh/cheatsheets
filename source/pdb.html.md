---
title: Pdb
category: Debuggers
---

## Commands

+---------+---------+
| Command | Meaning |
+---------+---------+
| `h`     | List commands |
| `d`/`u` | Move down/up the stack |
| `s`     | Step (continue executing and stop when next possible) |
| `n`     | Next (continue executing to the next line in the current function) |
| `r`     | Return (continue executing until the current function returns) |
| `c`     | Continue (until the next breakpoint) |
| `l`     | List source code |
| `a`     | Args of the current function |
| `p`/`pp` | Print/prettyprint an expression |
+---------+---------+
