---
name: Internet Explorer
category: Tools
---

# Setting up the IE VMs

- Use VMware Fusion/Workstation. They're miserable to use on Virtualbox.
- [Download the VMs from Microsoft](https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/).
- Open the .ovf files with Fusion.
- Add a "Private to my Mac" (host-only) network interface.
- On the Mac, run `ifconfig vmnet1` to find out the address of the Mac.
- Listen on that interface as well as localhost (or `0.0.0.0`).